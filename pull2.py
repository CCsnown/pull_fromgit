# -*- coding: utf-8 -*-
'''
需求：每隔一小时访问git，若仓库有更新，则从远程仓库拉取最新代码；若无更新，不作处理
思路：
1、获取最新的commitid，若与上一次保存的id不一致，则拉取代码
2、对git的操作，使用第三方库GitPython
3、通过定时器来间隔执行函数
'''
import git
import os
import shutil
import threading
from config import read_config


class refreshCode:
    '''
    _commitid：上一次保存的commitid
    sourceSrcDir：本地仓库地址
    dstResDir：备份文件地址
    remote_git_path:远程仓库地址
    '''
    def __init__(self):
        self._commitid = ''
        self.sourceSrcDir = read_config.sourceDir
        self.dstResDir = read_config.dstDir
        self.remote_git_path = read_config.remote_git_path

    def check_commit(self):
        '''
        通过比对commitid是否一致，来判断是否执行拉取代码的操作
        :return: none
        '''
        #初始化仓库，并获取最新的commitid
        repo = git.Repo.init(path=self.sourceSrcDir)
        headcommit = repo.head.commit

        # 最新的commitid与上次保存的commitid不一致
        if self._commitid != headcommit:
            # 远程交互，拉取代码，
            remote_git = repo.remote()
            remote_git.fetch()
            remote_git.pull()

            # 克隆仓库，若已经存在目的文档，先删除，再进行备份；并保存最新的commitid
            # shutil.copytree(sourceSrcDir, dstResDir)
            if os.path.exists(self.dstResDir):
                shutil.rmtree(self.dstResDir)
            repo = git.Repo.clone_from(url=self.remote_git_path, to_path=self.dstResDir)
            self._commitid = headcommit
            print('success')
        else:#commitid一致，不作处理
            print('nothing to commit, working tree clean')

def myTimer(fobj):
    '''
    函数内部直接调用refreshCode类中的check_commit方法
    :param fobj: refreshCode对象
    :return: none
    '''
    fobj.check_commit()
    global timer
    timer = threading.Timer(1800.0, myTimer, [fobj])
    timer.start()


if __name__ == "__main__":
    fresh = refreshCode()
    fresh.check_commit()
    timer = threading.Timer(1800.0, myTimer, [fresh])
    timer.start()





