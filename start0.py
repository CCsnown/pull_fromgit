# -*- coding: utf-8 -*-

import threading
from pull3 import refreshCode


def timer1H(fobj):
    fobj.check_commit()
    global timer
    timer = threading.Timer(1800.0, timer1H, [fobj])
    timer.start()


if __name__ == "__main__":
    fresh = refreshCode()
    fresh.check_commit()
    timer = threading.Timer(1800.0, timer1H, [fresh])
    timer.start()


