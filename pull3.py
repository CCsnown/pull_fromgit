# -*- coding: utf-8 -*-

'''
需求：每隔一小时访问git，若仓库有更新，则从远程仓库拉取最新代码；若无更新，不作处理
思路：
1、获取最新的commitid，若与上一次保存的id不一致，则拉取代码
2、对git的操作，使用模块subprocess
3、通过定时器来间隔执行函数
'''

import subprocess
from config import read_config


class refreshCode:
    def __init__(self):
        '''
        _commitid：上一次保存的commitid
        sourceSrcDir：本地仓库地址
        dstResDir：备份文件地址
        '''
        self.__commitid = ''
        self.__sourceSrcDir = read_config.sourceDir
        self.__dstResDir = read_config.dstDir

    def check_commit(self):
        p = subprocess.Popen("git log -1 |grep commit | awk {'print $2'}", stdout=subprocess.PIPE, shell=True, cwd=self.__sourceSrcDir, universal_newlines=True)
        commitid = p.stdout.read().strip()
        print(commitid)
        if commitid != self.__commitid:#最新的commitid与上次保存的commitid不一致，拉取代码
            subprocess.Popen('git pull', stdout=subprocess.PIPE, shell=True, cwd=self.__sourceSrcDir)
            self.__commitid = commitid
            print('success')
            cmd_chmod = 'chmod -R 777 %s;chmod 777 %s;' % (self.__sourceSrcDir, self.__dstResDir)
            cmd = cmd_chmod + 'cp -rp ' + self.__sourceSrcDir + '/. ' + self.__dstResDir
            subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True, cwd=self.__sourceSrcDir)
        else:
            print('nothing to commit, working tree clean')


if __name__ == "__main__":
    fresh = refreshCode()
    fresh.check_commit()

