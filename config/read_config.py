# -*- coding: utf-8 -*-
import configparser
import os


cf = configparser.ConfigParser()
# print(os.path.join(os.getcwd(), 'config.ini'))
#获取文件的当前路径（绝对路径）
cur_path=os.path.dirname(os.path.realpath(__file__))
# print(cur_path)
#获取config.ini的路径
config_path = os.path.join(cur_path,'config.ini')
cf.read(config_path)

secs = cf.sections()   # 获取文件中所有的section(一个配置文件中可以有多个配置，如path、backup
# print(secs)

options = cf.options("path")  # 获取某个section名为local_git_path、remote_git_path
# print(options)

items = cf.items("path")  # 获取section名为path模块所对应的全部键值对
# print(items)

local_git_path = cf.get("path", "local_git_path")  # 获取[path]中local_git_path对应的值
# print(local_git_path)

remote_git_path = cf.get("path", "remote_git_path")
# print(remote_git_path)

sourceDir = cf.get("backup", "sourceDir")
# print(sourceDir)

dstDir = cf.get("backup", "dstDir")
# print(dstDir)

